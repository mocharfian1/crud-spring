FROM openjdk:20

WORKDIR /app

CMD ["java", "-jar", "app.jar"]
